package com.fintech.unitask.errorhandling;

import org.springframework.http.HttpStatus;

public enum Errors implements ErrorResponse {
    DEMO_ERROR("DEMO_ERROR", HttpStatus.UNAUTHORIZED, "Authentication failed for user {user} "),
    PIN_ALREADY_EXIST("PIN_ALREADY_EXIST",HttpStatus.BAD_REQUEST, "Pin already exist!"),
    USER_NOT_FOUND("USER_NOT_FOUND", HttpStatus.NOT_FOUND, "User not found");



    String key;

    HttpStatus httpStatus;

    String message;

    Errors(String key, HttpStatus httpStatus, String message) {
        this.message = message;
        this.key = key;
        this.httpStatus = httpStatus;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}











