package com.fintech.unitask.dto;

import lombok.Data;

@Data
public class AccountDto {

    private Long id;

    private String name;

    private Double balance;
}
