package com.fintech.unitask.dto;

import lombok.Data;

@Data
public class TransferDto {

    private Long fromAccount;

    private Long toAccount;

    private Double amount;
}
