package com.fintech.unitask.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
public class LoginDto {

    @NotBlank
    @Size(min = 7, max = 7)
    private String pin;

    @NotBlank
    private String password;
}
