package com.fintech.unitask.dto;

import lombok.Data;

@Data
public class CurrencyRateDto {

    private Long id;

    private String currency;

    private Double amount;
}
