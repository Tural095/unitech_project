package com.fintech.unitask.enums;

public enum AccountStatus {
    ACTIVE, INACTIVE
}
