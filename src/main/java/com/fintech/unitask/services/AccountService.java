package com.fintech.unitask.services;

import com.fintech.unitask.dto.AccountDto;
import com.fintech.unitask.dto.TransferDto;
import com.fintech.unitask.dto.TransferResultDto;

import java.util.List;

public interface AccountService {

    List<AccountDto> getUserAccounts(Long userId);

    TransferResultDto transfer(Long userId, TransferDto transferDto);

}
