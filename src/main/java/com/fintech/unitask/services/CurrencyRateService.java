package com.fintech.unitask.services;

import com.fintech.unitask.dto.CurrencyRateDto;

public interface CurrencyRateService {

    CurrencyRateDto showCurrencyRate(CurrencyRateDto currencyRateDto);



}
