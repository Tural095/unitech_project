package com.fintech.unitask.services;

import com.fintech.unitask.dto.LoginDto;
import com.fintech.unitask.dto.RegisterDto;

public interface UserService {

    RegisterDto register(RegisterDto registerDto);

    LoginDto login(LoginDto loginDto);
}
