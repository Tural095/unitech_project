package com.fintech.unitask.services.impl;

import com.fintech.unitask.domain.User;
import com.fintech.unitask.dto.LoginDto;
import com.fintech.unitask.dto.RegisterDto;
import com.fintech.unitask.errorhandling.ApplicationException;
import com.fintech.unitask.errorhandling.Errors;
import com.fintech.unitask.exception.InvalidCredentialsException;
import com.fintech.unitask.repository.UserRepository;
import com.fintech.unitask.services.UserService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;

    @Override
    public RegisterDto register(RegisterDto registerDto) {
        Optional<User> existingUser = userRepository.findByPin(registerDto.getPin());
        if (existingUser.isPresent()) throw new ApplicationException(Errors.PIN_ALREADY_EXIST);

        User newUser = modelMapper.map(registerDto, User.class);
        newUser.setUsername(registerDto.getPin());
        newUser.setPassword(passwordEncoder.encode(registerDto.getPassword()));
        newUser.setAccountNonExpired(true);
        newUser.setAccountNonLocked(true);
        newUser.setEnabled(true);
        newUser.setCredentialsNonExpired(true);


        userRepository.save(newUser);

        // TODO: return JWT
        return registerDto;
    }

    @Override
    public LoginDto login(LoginDto loginDto) {
        User user = userRepository.findByPinAndPassword(loginDto.getPin(), loginDto.getPassword())
                .orElseThrow(() -> new InvalidCredentialsException("Pin or password is invalid!"));

        // TODO: return JWT
        return modelMapper.map(user, LoginDto.class);
    }
}
