package com.fintech.unitask.services.impl;

import com.fintech.unitask.errorhandling.ApplicationException;
import com.fintech.unitask.errorhandling.Errors;
import com.fintech.unitask.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(()-> new ApplicationException(Errors.USER_NOT_FOUND));
    }
}
