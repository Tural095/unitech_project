package com.fintech.unitask.controller;

import com.fintech.unitask.dto.LoginDto;
import com.fintech.unitask.dto.RegisterDto;
import com.fintech.unitask.services.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RequiredArgsConstructor
@RestController
@RequestMapping
public class UserController {

    private final UserService userService;

    @PostMapping("/register")
    public RegisterDto registerUser(@RequestBody @Valid RegisterDto registerDto) {
        return userService.register(registerDto);
    }

    @PostMapping("/login")
    public LoginDto loginUser(@RequestBody @Valid LoginDto loginDto) {
        return userService.login(loginDto);
    }
}

