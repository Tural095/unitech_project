package com.fintech.unitask.repository;

import com.fintech.unitask.domain.User;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByPin(String pin);

    Optional<User> findByPinAndPassword(String pin, String password);

    @EntityGraph(attributePaths = "authorities")
    Optional<User> findByUsername(String userName);

}
