package com.fintech.unitask.domain;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "currency_rates")
public class CurrencyRate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String sourceCurrency;

    @NotNull
    private BigDecimal amountToConvert;

    private BigDecimal convertedAmount;

    @NotBlank
    private String targetCurrency;
}
