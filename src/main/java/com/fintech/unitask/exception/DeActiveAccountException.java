package com.fintech.unitask.exception;

public class DeActiveAccountException extends RuntimeException{

    public DeActiveAccountException(String message){
        super(message);
    }
}
