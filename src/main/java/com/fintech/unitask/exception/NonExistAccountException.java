package com.fintech.unitask.exception;

public class NonExistAccountException extends RuntimeException{

    public NonExistAccountException(String message){
        super(message);
    }
}
