package com.fintech.unitask.exception;

public class PinAlreadyExistException extends RuntimeException {

    public PinAlreadyExistException(String message) {
        super(message);
    }
}
